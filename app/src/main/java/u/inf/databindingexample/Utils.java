package u.inf.databindingexample;

import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

public class Utils {

    private static DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();

    static int calculatePixels(int dp) {
        float ratio = (float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT;
        return Math.round(dp * ratio);
    }

    static int getScreenWidth() {
        return metrics.widthPixels;
    }

    static int generateRandomColor() {
        Random random = new Random();
        return Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView imageView, @NonNull String stringURL) {
        URL url;
        try {
            url = new URL(stringURL);
            new LoadImageTask(url, imageView).execute();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        }
    }

    /*@BindingAdapter("randomTextColor")
    public static void setRandomTextColor(TextView textView, int color) {
        textView.setTextColor(generateRandomColor());
    }*/

    public static class LoadImageTask extends AsyncTask<Void, Void, Bitmap> {

        private URL url;
        private WeakReference<ImageView> imageView;

        LoadImageTask(URL url, ImageView imageView) {
            this.url = url;
            this.imageView = new WeakReference<>(imageView);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((MainActivity) imageView.get().getContext()).showDialog();
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            Bitmap bmp;
            try {
                assert url != null;
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                return bmp;
            } catch (IOException e) {
                Log.e("Image loading failed", "Check network connection and website availability!");
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(bitmap != null) {
                imageView.get().setImageBitmap(bitmap);
            } else {
                imageView.get().setImageResource(R.drawable.ic_error);
                imageView.get().setBackgroundResource(R.drawable.fallback_drawable);
                imageView.get().setVisibility(View.VISIBLE);
            }
            ((MainActivity) imageView.get().getContext()).dismissDialog();
        }
    }
}
