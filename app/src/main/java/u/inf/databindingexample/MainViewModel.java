package u.inf.databindingexample;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.graphics.Color;
import android.view.View;

public class MainViewModel extends BaseObservable {

    @Bindable
    private String text = "Press a button!";

    @Bindable
    private int color = Color.GRAY;

    @Bindable
    private int topViewVisibility = View.VISIBLE;

    private String url = "https://picsum.photos/" + Utils.getScreenWidth() +"/" + Utils.calculatePixels(150) + "/" + "?random";

    public void onClickButton(View view) {
        setText("Button pressed: " + view.getTag());
        setColor(Utils.generateRandomColor());
        switchTopViewVisibility();
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
        notifyPropertyChanged(BR.color);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        notifyPropertyChanged(u.inf.databindingexample.BR.text);
    }

    public String getUrl() {
        return url;
    }

    public int getTopViewVisibility() {
        return topViewVisibility;
    }

    private void switchTopViewVisibility() {
        topViewVisibility = getOppositeVisibility(topViewVisibility);
        notifyPropertyChanged(BR.topViewVisibility);
    }

    private int getOppositeVisibility(int visibility) {
        return (visibility == View.VISIBLE) ? View.GONE : View.VISIBLE;
    }
}
